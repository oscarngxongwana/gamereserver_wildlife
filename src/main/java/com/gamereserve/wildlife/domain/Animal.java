package com.gamereserve.wildlife.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Table
public class Animal implements Serializable {

    @Id
    private String id = UUID.randomUUID().toString();
    @Column
    private String type;
    @OneToMany
    private List<GeoLocation> geoLocation;

    public Animal(String type, List<GeoLocation> geoLocation) {
        this.type = type;
        this.geoLocation = geoLocation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<GeoLocation> getGeoLocation() {
        return geoLocation;
    }

    public void setGeoLocation(List<GeoLocation> geoLocation) {
        this.geoLocation = geoLocation;
    }
}
