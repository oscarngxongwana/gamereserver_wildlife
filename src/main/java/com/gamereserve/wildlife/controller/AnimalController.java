package com.gamereserve.wildlife.controller;

import com.gamereserve.wildlife.domain.Animal;
import com.gamereserve.wildlife.domain.GeoLocation;
import com.gamereserve.wildlife.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController("animal")
public class AnimalController {

    @Autowired
    private AnimalService animalService;

    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public String addAnimal(@Valid Animal animal) {
        animalService.addAnimal(animal);
        return "Successful";
    }

    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(name = "{animalId}", method = RequestMethod.PUT, produces = "application/json", consumes = "application/json")
    public String addGpsLocation(@PathVariable String animalId, @RequestBody GeoLocation geoLocation) {
        geoLocation.setTimestamp(new Date());
        animalService.updateAnimalLocation(animalId, geoLocation);
        return "Successful";
    }

    @Transactional(readOnly = true)
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(name = "{animalId}", method = RequestMethod.GET, produces = "application/json", consumes = "application/json")
    public String getAnimal(@PathVariable String animalId) {
        animalService.getAnimal(animalId);
        return "Successful";
    }

    @Transactional(readOnly = true)
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, produces = "application/json", consumes = "application/json")
    public List<Animal> getAnimals(@RequestParam String type) {
        List<Animal> animals = animalService.getAnimals(type);
        return !CollectionUtils.isEmpty(animals) ? animals : new ArrayList<>();
    }
}
