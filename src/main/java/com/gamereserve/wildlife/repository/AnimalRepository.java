package com.gamereserve.wildlife.repository;

import com.gamereserve.wildlife.domain.Animal;
import org.springframework.data.repository.CrudRepository;

public interface AnimalRepository extends CrudRepository<Animal, String>{
}
