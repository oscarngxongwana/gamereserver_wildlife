package com.gamereserve.wildlife.service.impl;

import com.gamereserve.wildlife.domain.Animal;
import com.gamereserve.wildlife.domain.GeoLocation;
import com.gamereserve.wildlife.repository.AnimalRepository;
import com.gamereserve.wildlife.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnimalServiceImpl implements AnimalService {

    @Autowired
    AnimalRepository animalRepository;

    @Override
    public boolean addAnimal(Animal animal) {//TODO
        //save animal to the database
        animalRepository.save(animal);
        return false;
    }

    @Override
    public boolean updateAnimalLocation(String animalId, GeoLocation geoLocation) {//TODO
        //find animal
        Animal animal = animalRepository.findOne(animalId);
        List<GeoLocation> locations = animal.getGeoLocation();
        locations.add(geoLocation);
        animalRepository.save(animal);
        return true;
    }

    @Override
    public Animal getAnimal(String animalId) {//TODO
        return null;
    }

    @Override
    public List<Animal> getAnimals(String type) {//TODO
        return null;
    }
}
