package com.gamereserve.wildlife.service;

import com.gamereserve.wildlife.domain.Animal;
import com.gamereserve.wildlife.domain.GeoLocation;

import java.util.List;

public interface AnimalService {

    boolean addAnimal(Animal animal);

    boolean updateAnimalLocation(String animalId, GeoLocation geoLocation);

    Animal getAnimal(String animalId);

    List<Animal> getAnimals(String type);
}
